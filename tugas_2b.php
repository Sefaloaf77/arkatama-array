<?php
$data = file_get_contents("data_mahasiswa.json");
$data = json_decode($data, true);
$dataMahasiswa = $data['data'];

$mappedData = [
    "female" => [
        "count" => 0,
        "scores" => [
            "average" => 0,
            "min" => 0,
            "max" => 0
        ],
        "students" => array()
    ],
    "male" => [
        "count" => 0,
        "scores" => [
            "average" => 0,
            "min" => 0,
            "max" => 0
        ],
        "students" => array()
    ],
];

foreach ($dataMahasiswa as $i => $mahasiswa) {
    $key = "male";

    if ($mahasiswa['gender'] == "F") {
        $key = "female";
    };

    $mappedData[$key]['students'][] = $mahasiswa;
    //cari max 
    if($mahasiswa['score']>$mappedData[$key]['scores']['max']){
        $mappedData[$key]['scores']['max'] = $mahasiswa['score'];
    }

    //cari min
    if ($mappedData[$key]['count'] == 0) {
        $mappedData[$key]['scores']['min'] = $mahasiswa['score'];
        
        // average awal
        $mappedData[$key]['scores']['average'] = $mahasiswa['score'];
    } else{
        if ($mahasiswa['score'] < $mappedData[$key]['scores']['min']) {
            $mappedData[$key]['scores']['min'] = $mahasiswa['score'];
        }
        $count = $mappedData[$key]['count'];
        $average =  $mappedData[$key]['scores']['average'];

        $averageBaru = (($count * $average) + $mahasiswa['score'])/ ($count+1);
        $formatKoma = number_format($averageBaru,2); 

        $mappedData[$key]['scores']['average'] = $formatKoma;
        
    }
    $mappedData[$key]['count']++;
};

header("Content-Type: application/json");
echo json_encode($mappedData);
exit();
?>
