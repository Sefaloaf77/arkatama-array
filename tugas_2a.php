<?php
$data = file_get_contents("data_mahasiswa.json");
$data = json_decode($data, true);
$get_data = $data['data'];
// A	4.00	80 < N≤ 100
// B+	3.50	75<N≤80
// B	3.00	69<N≤75
// C+	2.50	60<N≤69
// C	2.00	55<N≤60
// D+	1.50	50<N≤55
// D	1.00	44<N≤50
// E	0.00	0≤N≤44
// $add_grade = $get_data[] = "grade";
//array_push($get_data[0],"grade"=>"70");

// cara 1
// foreach ($get_data as $row) {
//     if ($row['score'] <= 100 && $row['score'] > 80) {
//         $row['grade'] = "A";
//     } else if($row['score'] <= 80 && $row['score'] > 75){
//         $row['grade'] = "B+";
//     } else if ($row['score'] <= 75 && $row['score'] > 69){
//         $row['grade'] = "B";
//     } else if ($row['score'] <= 69 && $row['score'] > 60){
//         $row['grade'] = "C+";
//     } else if($row['score'] <= 60 && $row['score'] > 55){
//         $row['grade'] = "C";
//     } else if($row['score'] <= 55 && $row['score'] > 50){
//         $row['grade'] = "D+";
//     } else if($row['score'] <= 50 && $row['score'] > 44){
//         $row['grade'] = "D";
//     } else {
//         $row['grade'] = "E";
//     }
//     header("Content-Type: application/json");
//     echo json_encode($row);
//     
// };
// exit();


// cara 2

$hasil = array_map(function($v){
    $cek = "";
    if ($v['score'] <= 100 && $v['score'] > 80) {
        $cek = "A";
    } else if($v['score'] <= 80 && $v['score'] > 75){
        $cek = "B+";
    } else if ($v['score'] <= 75 && $v['score'] > 69){
        $cek = "B";
    } else if ($v['score'] <= 69 && $v['score'] > 60){
        $cek = "C+";
    } else if($v['score'] <= 60 && $v['score'] > 55){
        $cek = "C";
    } else if($v['score'] <= 55 && $v['score'] > 50){
        $cek = "D+";
    } else if($v['score'] <= 50 && $v['score'] > 44){
        $cek = "D";
    } else {
        $cek = "E";
    } 
    $v['grade']=$cek;
    return ($v);
}, $get_data);

    header("Content-Type: application/json");
    echo json_encode($hasil);
    exit();