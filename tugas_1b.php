<?php
$data = file_get_contents("desa_jatim.json");
$data = json_decode($data, true);
$get_data = $data['data'];

// cara 1
// $temp = array(
//     "id:"=>$get_data['kode_desa'],
//     "text:"=>$get_data['nama_desa'], 
//     "data:"=>$get_data
// );

// cara 2
// $temp=[];
// foreach($get_data as $key => $cek){
//     $temp[$key]['id']=$cek['kode_desa'];
//     $temp[$key]['text']=$cek['nama_desa']."-".$cek['nama_kecamatan']."-".$cek['nama_kab_kota']."-".$cek['nama_provinsi'];
//     $temp[$key]['data']=$cek;
    
// };
// header("Content-Type: application/json");
// echo json_encode($hasil);
// exit();

// cara 3
$hasil = array_map(function($v){
    return [
        'id'=>$v['kode_desa'],
        'text'=>$v['nama_desa']."-".$v['nama_kecamatan']."-".$v['nama_kab_kota']."-".$v['nama_provinsi'],
        'data'=>$v
    ];
}, $get_data);
header("Content-Type: application/json");
echo json_encode($hasil);
exit();
?>
