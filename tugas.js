// Tugas Array
// Aturan:
// - Menggunakan PHP
// - Ubah file .JSON menjadi PHP array
// - Hasil akhir harus bisa mengembalikan file format JSON saat diakses melalui browser
// - Buat satu file dokumen yang menjelaskan masing" fungsi array yang digunakan dalam mnegerjakan
//     misal: array_sort(), array_filter() dsb 
// - Simpan di repository masing" dan undang riezq.25@gmail.com 
// - Struktur Folder:
// - tugas1
    // - desa_jatim.json
    // - a.php
    // - b.php
    // - c.php
// - tugas2
    // - data_mahasiswa.json
    // - a.php
    // - b.php
// - laporan.docx

// 1. Ubah data desa_jatim menjadi format
// a. urutkan nama desa secara ascending
// b.
let data1 = [
  {
    id: 350101001,
    text: "Nama Desa - Nama Kecamatan - Nama Kabupaten - Nama Provinsi",
    data: {
      kode_desa: "350101001",
      nama_desa: "WIDORO",
      kode_kec: "3501010",
      nama_kecamatan: "DONOROJO",
      kode_kab_kota: "3501",
      nama_kab_kota: "KABUPATEN PACITAN",
      kode_prop: "3500",
      nama_provinsi: "JAWA TIMUR",
    },
  },
  {
    id: 350101001,
    text: "Nama Desa - Nama Kecamatan - Nama Kabupaten - Nama Provinsi",
    data: {
      kode_desa: "350101001",
      nama_desa: "WIDORO",
      kode_kec: "3501010",
      nama_kecamatan: "DONOROJO",
      kode_kab_kota: "3501",
      nama_kab_kota: "KABUPATEN PACITAN",
      kode_prop: "3500",
      nama_provinsi: "JAWA TIMUR",
    },
  },
];

// c.
let data2 = {
  kode_prop: 3500,
  nama_propinsi: "JAWA TIMUR",
  cities: [
    {
      kode_kab_kota: 3501,
      nama_kab_kota: "KABUPATEN PACITAN",
      subdisticts: [
        {
          kode_kec: 3501010,
          nama_kecamatan: "DONOROJO",
          villages: [
            {
              kode_desa: 350101001,
              nama_desa: "WIDORO",
            },
            {
              kode_desa: 350101002,
              nama_desa: "SOME VILAGE",
            },
          ],
          kode_kec: 3501011,
          nama_kecamatan: "Some District",
          villages: [
            {
              kode_desa: 350101001,
              nama_desa: "WIDORO",
            },
            {
              kode_desa: 350101002,
              nama_desa: "SOME VILAGE",
            },
          ],
        },
      ],
    },
    {
      kode_kab_kota: 3502,
      nama_kab_kota: "Some City",
      subdisticts: [
        {
          kode_kec: 3501010,
          nama_kecamatan: "DONOROJO",
          villages: [
            {
              kode_desa: 350101001,
              nama_desa: "WIDORO",
            },
            {
              kode_desa: 350101002,
              nama_desa: "SOME VILAGE",
            },
          ],
          kode_kec: 3501011,
          nama_kecamatan: "Some District",
          villages: [
            {
              kode_desa: 350101001,
              nama_desa: "WIDORO",
            },
            {
              kode_desa: 350101002,
              nama_desa: "SOME VILAGE",
            },
          ],
        },
      ],
    },
  ],
};


// 2. Ubah data mahasiswa menjadi format
// a.
// A	4.00	80 < N≤ 100
// B+	3.50	75<N≤80
// B	3.00	69<N≤75
// C+	2.50	60<N≤69
// C	2.00	55<N≤60
// D+	1.50	50<N≤55
// D	1.00	44<N≤50
// E	0.00	0≤N≤44

let data3 = [
  {
    id: 0,
    name: "Tama Howe",
    gender: "F",
    score: 69,
    grade: "B",
  },
];

// b.
let data4 = {
  female: {
    count: 23,
    score: {
      average: 78,
      min: 68,
      max: 89,
    },
    students: [
      {
        id: 0,
        name: "Tama Howe",
        gender: "F",
        score: 69,
        grade: "B",
      },
    ],
  },
  male: {
    count: 23,
    score: {
      average: 78,
      min: 68,
      max: 89,
    },
    students: [
      {
        id: 3,
        name: "Tama Howe",
        gender: "M",
        score: 69,
        grade: "B",
      },
    ],
  },
};
