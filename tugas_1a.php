<?php
    $data=file_get_contents("desa_jatim.json");
    $data =json_decode($data,true);
    $get_data= $data['data'];
    //sortir data berdasarkan nama desa
    usort($get_data, function($a,$b){
        return strcmp($a["nama_desa"],$b["nama_desa"]);
    });

    //menampilkan data dalam format JSON
    header("Content-Type: application/json");
    echo json_encode($get_data);
    exit();
?>